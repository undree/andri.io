import os
import shutil
import pathlib

TEMPLATE_FILE = "template.html"
CONTENT_DIR = "content/"
STATIC_DIR = "static/"
DIST_DIR = "dist/"

# Remove dist diirectory and/or create empty dist directory
if os.path.exists(DIST_DIR):
    shutil.rmtree(DIST_DIR)
pathlib.Path(DIST_DIR).mkdir()

# Move static files to dist directory
for static_file in os.listdir(STATIC_DIR):
    shutil.copy(STATIC_DIR + static_file, DIST_DIR)

# Load template where content will be placed
with open(TEMPLATE_FILE) as file:
    template = file.read()

# Recursively find HTML files in content directory
for file in pathlib.Path(CONTENT_DIR).rglob("*.html"):
    content_file_path = str(file)
    dist_file_path = content_file_path.replace(CONTENT_DIR, DIST_DIR)
    dist_file_dir = os.path.dirname(dist_file_path)
    parent_dir = dist_file_dir[len(DIST_DIR.rstrip("/")):] or "/"

    with open(content_file_path) as content_file:
        content = content_file.read()

    # Replace content with HTML from content file
    dist_file_content = template.replace("{{ content }}", content)

    # Replace path in title with parent directory
    dist_file_content = dist_file_content.replace("{{ path }}", parent_dir)

    # Replace nav link to current directory with active class
    dist_file_content = dist_file_content.replace(
        f"<a href=\"{parent_dir}\">",
        "<a class=\"active\">"
    )

    # Make directories in the dist directory in case they don't exist
    pathlib.Path(dist_file_dir).mkdir(parents=True, exist_ok=True)

    # Save generated HTML file to the dist directory
    with open(dist_file_path, "w") as dist_file:
        dist_file.write(dist_file_content)

